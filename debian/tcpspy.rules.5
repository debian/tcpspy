.\" This file is part of tcpspy Debian package, a TCP/IP connection monitor.
.\" In truth this is just an snipped version of tcpspy manpage, which is
.\" copyrighted by Tim J. Robbins under the same terms below.
.\"
.\" Copyright (c) 2001 Pablo Lorenzzoni.
.\" Copyright (c) 2011 Mats Erik Andersson
.\" All rights reserved.
.\" 
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. The name of the author may not be used to endorse or promote products
.\"    derived from this software without specific prior written permission.
.\" 
.\" THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
.\" INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
.\" AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
.\" THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
.\" EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
.\" PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
.\" OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
.\" WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
.\" OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
.\" ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.\"
.\" $Id: tcpspy.rules.5,v 1.33 2001/01/28 13:09:12 fyre Exp $
.TH TCPSPY.RULES 5 "April 2001" Spectra "tcpspy.rules"
.SH NAME
tcpspy.rules \- configuration file for tcpspy
.SH DESCRIPTION
.LP
This file, by default
.IR /etc/tcpspy.rules ,
is read by the
.IR /etc/init.d/tcpspy 
script at init time in order to configure
tcpspy (see tcpspy(8)) logger filtering rules.
.LP
It might look like:
.IP
.nf
# /etc/tcpspt.rules example
user "joedoe" and rport 22 and raddr 192.168.1.10
user 1003
lport 22 or lport 21
(lport 23 and user "joedoe") or raddr 192.168.1.20
.fi
.LP
This rules file specifies that tcpspy logs tcp connections according
to 4 rules (line 1 to line 4 - one per each line) using the boolean
logic (see below) to evaluate each rule.
.LP
This particular example logs connections:
.TP
line 1 - for user "joedoe" connecting to 192.168.1.10:22 (remote)
.TP
line 2 - for user whose UID is 1003
.TP
line 3 - to *:22 or *:21 (both locally)
.TP
line 4 - for user "joedoe" to *:23 (local) or to 192.168.1.20 (remote)
.LP
Everything from an "#" signal and the end of the line will not be evaluated.
.LP
.PP
.SS "Rule Syntax - just extracted from tcpspy(8)"
A rule may be specified with the following comparison operators:
.TP
.BI user " uid"
True if the local user initiating or accepting the connection has the
.B effective
user id
.IR uid .
.TP
.BI user " \N'34'username\N'34'"
Same as above, but using a username instead of a user id.
.TP
.BI ip
True if the connection is IPv4.
.TP
.BI ip6
True if the connection is IPv6.
.TP
.BI lport " port"
True if the local end of the connection has port number
.IR port .
.TP
.BI lport " [low] - [high]"
True if the local end of the connection has a port number
greater than or equal to
.I low
and less than or equal to
.IR high .
If the form
.I low-
is used, high is assumed to be 65535.
If the form
.I -high
is used, low is assumed to be 0. It is an error to omit both
.IR low " and " high .
.TP
.BI lport " \N'34'service\N'34'"
Same as above, but using a service name from
.I /etc/services
instead of a port number.
.TP
.B rport
Same as
.B lport
but compares the port number of the remote end of the connection.
.TP
.BI laddr " n.n.n.n[/m.m.m.m]"
.TP
.BI laddr " n.n.n.n/m"
.TP
.BI laddr " ip6-addr[/m]"
Interpreted as a "net/mask" expression; true if "net" is equal to the bitwise
AND of the local address of the connection and "mask". If no mask is specified,
a default mask with all bits set (255.255.255.255) is used. The CIDR type netmask
is also possible. With IPv6 only a prefix length netmask is allowed, and the
length defaults to 128. Depending on the address family, these rules contain
an implicit match condition "ip" or "ip6", respectively.
.TP
.B raddr
Same as
.B laddr
but compares the remote address.
.TP
.BI exe " \N'34'pattern\N'34'"
True if the full filename (including directory) of the executable that 
created/accepted the connection matches
.IR pattern ,
a
.BR glob (7)-style
wildcard pattern.
.IP
The pattern "" (an empty string) matches connections created/accepted by 
processes whose executable filename is unknown.
.IP
If the
.B -p
option is not specified, a warning message will be printed, and the result of
this comparison will always be true.
.PP
Expressions (including the  comparisons listed above) may be joined together
with the following logical operations:
.TP
.IB expr1 " or " expr2
True if either of
.I expr1
or
.I expr2
are true (logical OR).
.TP
.IB expr1 " and " expr2
True if both
.I expr1
and
.I expr2
are true (logical AND).
.TP
.BI not " expr"
True if
.I expr
is false (logical NOT).
.PP
Rules are evaluated from left to right. Whitespace (space, tab and newline)
characters are ignored between "words". Rules consisting of only whitespace
match no connections, but do not cause an error.
Parentheses, '(' and ')' may be placed around expressions to affect the order
of evaluation.
.PP
.SS "Examples"
.TP
These are some sample rules which further demonstrate how they are constructed:
.TP
.nf
user "joe" and rport "ssh"
.fi
Log connections made by user "joe" for the service "ssh".

.TP
.nf
not raddr 10.0.0.0/255.0.0.0 and rport 25 and (user "bob" or user "joe")
.fi
Log connections made by users "bob" and "joe" to remote port 25 on machines
not on a fictional "intranet".

.SH AUTHOR
Tim J. Robbins (tcpspy),
Pablo Lorenzzoni (this manpage) and  Mats Erik Andersson (changes for IPv6)

.SH SEE ALSO
.BR glob (7),
.BR proc (5),
.BR services (5),
.BR signal (7),
.BR syslog (3),
.BR tcpspy (8)
